# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define tarbasename       ldas-tools-framecpp
%define _docdir           %{_datadir}/doc/ldas-tools-%{version}
%define check_boost169 ( 0%{?rhel} && 0%{?rhel} <= 8 )
%define check_cmake3 ( 0%{?rhel} && 0%{?rhel} <= 7 )
%define check_framel ( 0%{?rhel} && 0%{?rhel} <= 7 )

Summary: LDAS tools libframecpp toolkit runtime files
Name: ldas-tools-framecpp
Version: 2.7.3
Release: 2%{?dist}
License: GPLv2+
URL: "https://wiki.ligo.org/Computing/LDASTools"
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: https://software.igwn.org/lscsoft/source/ldas-tools-framecpp-%{version}.tar.gz
Requires: ldas-tools-al >= 2.6.6
%if %{check_boost169}
Requires: boost169-program-options
%else
Requires: boost-program-options
%endif
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: openssl-devel
%if %{check_cmake3}
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
BuildRequires: doxygen
%if %{check_boost169}
BuildRequires: boost169-devel
BuildRequires: boost169-program-options
BuildRequires: boost169
%else
BuildRequires: boost-devel
BuildRequires: boost-program-options
BuildRequires: boost
%endif
BuildRequires: pkgconfig
BuildRequires: zlib-devel
%if %{check_framel}
BuildRequires: libframel-devel
%endif
Buildrequires: ldas-tools-cmake >= 1.2.2
BuildRequires: ldas-tools-al-devel >= 2.6.6
BuildRequires: rpm-build

%description
This provides the runtime libraries for the framecpp library.

%package devel
Requires: ldas-tools-al-devel >= 2.6.6
Requires: ldas-tools-framecpp = %{version}
%if %{check_boost169}
Requires: boost169-devel
%else
Requires: boost-devel
%endif
Group: Development/Scientific
Summary: LDAS tools libframecpp toolkit development files
%description devel
This provides the develpement files the framecpp library.

%package doc
Group: Development/Scientific
Summary: LDAS tools libframecpp documentation
%description doc
This provides the documentation for the framecpp library.

%package c
Requires: ldas-tools-framecpp = %{version}
Group: Application/Scientific
Summary: LDAS tools c wrapping of libframecpp
%description c
This provides the runtime libraries for the framecpp library.

%package c-devel
Requires: ldas-tools-framecpp-devel = %{version}
Group: Development/Scientific
Summary: LDAS tools libframec toolkit development files
%description c-devel
This provides the develpement files the framecpp library.

%prep

%setup -c -T -D -a 0 -n %{name}-%{version}

%build
%if %{check_boost169}
export BOOST_CONFIGURE_OPTIONS="-DBoost_NO_SYSTEM_PATHS=True -DBOOST_INCLUDEDIR=%{_includedir}/boost169/ -DBOOST_LIBRARYDIR=%{_libdir}/boost169/"
%else
export BOOST_CONFIGURE_OPTIONS="-DBOOST_INCLUDEDIR=%{_includedir}/boost/ -DBOOST_LIBRARYDIR=%{_libdir}"
%endif
%if %{check_cmake3}
export CMAKE_PROGRAM=cmake3
%else
export CMAKE_PROGRAM=cmake
%endif

${CMAKE_PROGRAM} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    ${BOOST_CONFIGURE_OPTIONS} \
    %{tarbasename}-%{version}

make VERBOSE=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%exclude %{_libdir}/libframecppc.so*
%{_bindir}/framecpp_*
%{_libdir}/libframecpp*.so.*

%files devel
%defattr(-,root,root)
%exclude %{_libdir}/libframecppc.*a
%{_includedir}/framecpp
%{_libdir}/libframecpp*.*a
%{_libdir}/libframecpp*.so
%{_libdir}/pkgconfig/framecpp_common.pc
%{_libdir}/pkgconfig/framecpp3.pc
%{_libdir}/pkgconfig/framecpp4.pc
%{_libdir}/pkgconfig/framecpp6.pc
%{_libdir}/pkgconfig/framecpp7.pc
%{_libdir}/pkgconfig/framecpp8.pc
%{_libdir}/pkgconfig/framecpp.pc

%files doc
%defattr(-,root,root)
%doc %{_docdir}

%files c
%defattr(-,root,root,-)
%{_libdir}/libframecppc.so.*

%files c-devel
%defattr(-,root,root)
%{_includedir}/framecppc
%{_libdir}/libframecppc.*a
%{_libdir}/libframecppc.so
%{_libdir}/pkgconfig/framecppc.pc

%changelog
* Tue Sep 21 2021 Edward Maros <ed.maros@ligo.org> - 2.7.3-1
- Built for new release

* Fri Aug 13 2021 Edward Maros <ed.maros@ligo.org> - 2.7.2-1
- Built for new release

* Tue Mar 9 2021 Edward Maros <ed.maros@ligo.org> - 2.7.1-1
- Built for new release

* Wed Aug 14 2019 Edward Maros <ed.maros@ligo.org> - 2.6.6-1
- Built for new release

* Tue Jan 08 2019 Edward Maros <ed.maros@ligo.org> - 2.6.5-1
- Built for new release

* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.4-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.3-1
- Built for new release

* Mon Feb 13 2017 Edward Maros <ed.maros@ligo.org>  - 2.5.6-1
- Built for new release

* Fri Jan 27 2017 Edward Maros <ed.maros@ligo.org> - 2.5.5-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Built for new release

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
